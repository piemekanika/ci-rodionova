/**
 * Created by piemekanika on 16.02.17.
 */

public class Caesar {
    private char[] alphabet = new char[255];
    private int alphabetSize = 255;

    private char symbolEncode(char symbol, int key) {
        int position;

        // находим позицию symbol в нашем алфавите
        for (position = 0; position < alphabetSize; position++)
            if (alphabet[position] == symbol)
                break;

        // делаем сдвиг
        position += key;
        position %= alphabetSize;
        if (position < 0)
            position = alphabetSize + position;

        return alphabet[position];
    }

    public String encode(String stringToEncode, int key) {
        String result = "";

        // используем symbolEncode для каждого символа в строке
        for (int i = 0; i < stringToEncode.length(); i++)
            result += symbolEncode(stringToEncode.toCharArray()[i], key);

        return result;
    }

    public Caesar(String alphabet) {
        // перекидываем переданный алфавит из строки в массив char'ов
        int i;
        for (i = 0; i < alphabet.length(); i++)
            this.alphabet[i] = alphabet.toCharArray()[i];
        alphabetSize = i;
    }
}
