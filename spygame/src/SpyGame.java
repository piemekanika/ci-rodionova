/**
 * Created by piemekanika on 16.02.17.
 */

import java.util.Objects;
import java.util.Scanner;

public class SpyGame {
    public static void main(String args[]) {
        System.out.println("you're welcome.");

        // закидываем англ алфавит + 4 символа ( ,.?)
        Caesar caesar = new Caesar("abcdefghijklmnopqrstuvwxyz ,.?");

        String input = "";
        Scanner in = new Scanner(System.in);
        int key = 2;
        while (!Objects.equals(input, "ex")) {
            input = in.nextLine();
            System.out.println(caesar.encode(input, key));
        }
    }
}
